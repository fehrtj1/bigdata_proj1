# Tyler Fehr & Wei-en Lu
# Dr. Lory Al Moakar
# COMP 344 - Big Data
# Task2: Importing traffic data into DynamoDB.

from __future__ import print_function
from encoder import DecimalEncoder
# use python's pre-made libraries for our task
import boto3
import csv
import json
import decimal

name = 'Traffic_Violations_50'

# establish connection to dynamodb
dynamodb = boto3.resource('dynamodb', region_name='us-west-2', endpoint_url='http://localhost:8000')

# reference our table
table = dynamodb.Table(name)

## initialize file pointers
csvfile = open(name + '.csv', 'r')
#jsonfile = open(name + '.json', 'w')

# json keys for interpreting csv file, and then writing
jsonkeys = ("Date_Of_Stop","Time_Of_Stop","Agency","SubAgency","Description","Location","Latitude","Longitude","Accident","Belts","Personal_Injury","Property_Damage","Fatal","Commercial_License","HAZMAT","Commercial_Vehicle","Alcohol","Work_Zone","State","VehicleType","Year","Make","Model","Color",
            "Violation Type","Charge","Article","Contributed_To_Accident","Race","Gender","Driver_City","Driver_State","DL_State","Arrest_Type","Geolocation")

reader = csv.DictReader( csvfile, jsonkeys )

# create our output from parsing through the CSV file
#output = json.dumps( [ row for row in reader ] )

#insert each row into dynamoDB
print("Start inserting ", name, "...")
for row in reader:
    item = {}
    counter = 0
    for j in range(0,36):
        i = j-1
        # We will create a unique sort key.
        if j == 0:
            # 0 is date, 1 is time, 22 is model, 25 is charge
            item["Stop_ID"] = row[jsonkeys[0]] + " " + row[jsonkeys[1]] + " " + row[jsonkeys[22]] + " " + row[jsonkeys[25]]
        # make year an integer
        elif row[jsonkeys[i]] != '' and row[jsonkeys[i]] != 'Year' and jsonkeys[i] == 'Year':
            item[jsonkeys[i]] = int(row[jsonkeys[i]])
        # make latitude a decimal. We cannot make this a float
        # since dynamoDB cannot take in a float
        elif row[jsonkeys[i]] != '' and row[jsonkeys[i]] != 'Latitude' and jsonkeys[i] == 'Latitude':
            item[jsonkeys[i]] = decimal.Decimal(row[jsonkeys[i]])
        # make longitude a decimal
        elif row[jsonkeys[i]] != '' and row[jsonkeys[i]] != 'Longitude' and jsonkeys[i] == 'Longitude':
            item[jsonkeys[i]] = decimal.Decimal(row[jsonkeys[i]])
        # the rest of the items will be strings
        elif row[jsonkeys[i]] != '':
            item[jsonkeys[i]] = row[jsonkeys[i]]

    # put the item into the table
    table.put_item(Item = item)

print("Insert ", name, " all done!")
