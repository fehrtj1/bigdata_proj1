# Tyler Fehr & Wei-en Lu
# Dr. Lory Al Moakar
# COMP 344 - Big Data
# Task3: Analyze our imported data

from __future__ import print_function

# helper class for reading JSON data from aws
from encoder import DecimalEncoder

# use python's pre-made libraries for our task
import boto3
import json
import decimal
from boto3.dynamodb.conditions import Key, Attr
from boto3 import resource

# functional programming methods
from functools import reduce
from collections import Counter

name = 'Traffic_Violations_50'

dynamodb = boto3.resource("dynamodb", region_name="us-west-2", endpoint_url="http://localhost:8000")
table = dynamodb.Table(name)

def findByDate(searchDate):
    # store the returned results in a dictionary(JSON) and print them in a nice table format
    print("Traffic violations committed on " + searchDate)

    response = table.query(
        ProjectionExpression= "Description, Time_Of_Stop, SubAgency",
        # query Date_Of_Stop for the desired date
        KeyConditionExpression = Key('Date_Of_Stop').eq(searchDate),
    )

    header = ["Time_Of_Stop", "Description","SubAgency"]

    # print headers
    for key in header:
        print('{:<100}'.format(key),end='')
    print()
    # print each result
    for violation in response[u'Items']:
        for key in violation:
            print('{:<100}'.format(violation[key]),end='')

def countOutOfStateCars():
    response = table.scan(
            # look for State that is not equal MD and XX
            FilterExpression = Attr("State").ne("MD") & Attr("State").ne("XX") & Attr("State").ne("State")
        )
    # response's first tuple is the count after any filter expressions have been applied
    numCars = response['Count']

    print(numCars)
    return numCars

def findWorstOwners():
    # Calculate the number of violations per car make
    # Print the one with the most and return it from this function

    response = table.scan(ProjectionExpression = 'Make')

    car_makes = dict() # holds a (make, violations by that make) key-value pair

    #car_makes is passed by object
    addToDictionary(car_makes, response['Items']) # add first page of scan results

    # If the response contains 'LastEvaluatedKey', then there is more data we need to process
    # Process the entire scan through pagination, if appropriate
    while True:
        if response.get('LastEvaluatedKey'): # steps through the dynamodb table, page by page, and adds to dictionary
            response = table.scan(ProjectionExpression = 'Make', ExclusiveStartKey = response['LastEvaluatedKey'])
            addToDictionary(car_makes, response['Items'])
        else:
            break

    # Similar to Scala's foldLeft() functional method, we recursively find the max violation count
    # in the values from the key-value pairs from the dictionary we just created using a lambda function
    violationCount = reduce( lambda x, y: max(x, y), car_makes.values() )

    # Counter(car_makes).most_common(1) == [(most_common_make, number of occurrences)] use [0][0] to access first element in list, and then first element in tuple
    # returns the most_common(n) n tuples in key value pairs. We only want the most frequently occurring, so n = 1.
    makeOfWorstOwner = Counter(car_makes).most_common(1)[0][0]

    print(makeOfWorstOwner)
    print(violationCount)
    return makeOfWorstOwner

def addToDictionary(car_makes, data): # appends to car_makes dictionary, helper for findWorstOwners()
    # make dictionary of key values of the make, and the count of violations e.g. (FORD, 13)
    for i in data:
        if i['Make'] in car_makes:
            car_makes[ i['Make'] ] += 1 # if the car make is already in the dict, we increment it by one
        else:
            car_makes[ i['Make'] ] = 1 # if the car make is not in the dict, we add it to the dictionary, and add one (initialize it to 1)

# test our functions
findWorstOwners()
countOutOfStateCars()
findByDate('12/1/2014')
