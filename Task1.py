from __future__ import print_function # Python 2/3 compatibility
import boto3

name = 'Traffic_Violations_50'
dynamodb = boto3.resource('dynamodb', region_name='us-west-2', endpoint_url='http://localhost:8000')

def createTable():
    table = dynamodb.create_table(
       TableName= name,
       KeySchema=[
           {
               'AttributeName': 'Date_Of_Stop',
               'KeyType': 'HASH'  #Partition key
           },
           {
               'AttributeName': 'Stop_ID',
               'KeyType': 'RANGE'  #Sort key
           }
       ],
       AttributeDefinitions=[
           {
               'AttributeName': 'Date_Of_Stop',
               'AttributeType': 'S'
           },
           {
               'AttributeName': 'Stop_ID',
               'AttributeType': 'S'
           },

       ],

       ProvisionedThroughput={
           'ReadCapacityUnits': 5,

           'WriteCapacityUnits': 5
       }
    )
table = dynamodb.Table('name')
#createTable()
#table.delete()
print("Table status:", table.table_status)
#print(table.creation_date_time)
#print(table.item_count)
